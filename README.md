# DoorMan
First foray into the IoT with the [Onion Omega](https://onion.io/omega/).</br>
DoorMan takes input from [magnetic door sensors](https://www.adafruit.com/products/375) and emails the user if a door has been opened.
