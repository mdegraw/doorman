#! /usr/bin/python3

import smtplib
import subprocess

def send_mail():
    sender = "someone@gmail.com"
    recipent = "someoneelse@protonmail.com"
    password = "whaat"

    message = "Subject: Open Door"

    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(sender, password)

        server.sendmail(sender, recipent, message)
        print("Email sent!")

    except:
        print("Error Unable to send mail")

    server.quit()

class DoorMan(object):
    """A DoorMan Object monitors input from a sensor to detect if a door has been opened.
        door_pin refers to the gpio on an Onion Omega.
    """
    def __init__(self, door_pin='8'):
        self.door_open = False
        self.LED_PIN = '6'
        self.DOOR_PIN = door_pin
        subprocess.call(["fast-gpio", "set-output", self.LED_PIN])
        subprocess.call(["fast-gpio", "set-input", self.DOOR_PIN])

    def observe(self):
        door = subprocess.check_output(["fast-gpio", "read", self.DOOR_PIN]).decode(encoding='UTF-8')
        door = int(door[-2])

        if door < 1 and self.door_open == False:
            send_mail()
            subprocess.call(["sleep", "2"])
            self.door_open = True
            print("DOOR IS OPEN: {}".format(self.door_open))

        elif door > 0:
            self.door_open = False

        subprocess.call(["sleep", "1"])

def watch_doors(*args):
    """watch_doors takes as input the pin numbers on the dev board which correspond to door sensors"""

    doormen = [DoorMan(door_pin) for door_pin in args]
    print("Starting Observation")

    while True:
            for doorman in doormen:
                doorman.observe()


if __name__ == '__main__':
    watch_doors('8', '14')
