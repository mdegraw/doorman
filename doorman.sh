#! /bin/sh
# Simple Daemon for DoorMan
# Resides in /etc/init.d

case "$1" in
    start)
        echo "Starting Door Observation"
        python3 /usr/sbin/doorman.py &
        ;;

    stop)
        echo "Stopping Door Observation"
        killall python3
        ;;

    *)
        echo "Usage: /etc/init.d/doorman.sh {start|stop}"
        exit 1
        ;;
esac

exit 0
